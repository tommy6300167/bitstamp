FROM node:14
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000 3009
CMD ["node", "server.js"]