const router = require('koa-router')
const StreamService = require('../service/stream')
const server = new StreamService()

const commonRouter = new router()

commonRouter.get('/', server.wsStream)

module.exports = commonRouter
