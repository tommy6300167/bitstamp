const router = require('koa-router')
const DataService = require('../service/common')
const common = new DataService()

const commonRouter = new router()

commonRouter.get('/', common.fetchData)

module.exports = commonRouter
