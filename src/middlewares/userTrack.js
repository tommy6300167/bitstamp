// we use redis to record user's request info
// check user data
const { rataLimit, redisConfig } = require('../../config')
const redis = require('ioredis') // you need to fix version 3.1.2

const appInsightTrack = async (ctx, next) => {
  const ip = '123456'
  const userId = ctx.request.query.user
  try {
    const client = await redis.createClient({...redisConfig});

    const userIdKey = `userId:${userId}`
    const ipKey = `ip:${userId}`
    const multi = client.multi()
    multi.incr(userIdKey)
    multi.incr(ipKey)
    multi.expire(userIdKey, rataLimit.MINUTE_IN_MILLISECONDS / 1000) // New learn here, you can set expire time of the data
    multi.expire(ipKey, rataLimit.MINUTE_IN_MILLISECONDS / 1000)
    await multi.exec()

    multi.get(userIdKey)
    const userIdCount = await client.get(userIdKey)
    const ipCount = await client.get(ipKey)

    if (ipCount > rataLimit.MAX_IP_REQUESTS || userIdCount > rataLimit.MAX_USER_REQUESTS) {
      ctx.status = 429
      ctx.body = { ip: ipCount, id: userIdCount }
      return ctx
    } else {
      await next()
      // const remainingTime = rataLimit.MINUTE_IN_MILLISECONDS - (Date.now() % rataLimit.MINUTE_IN_MILLISECONDS);
      // await new Promise(resolve => setTimeout(resolve, remainingTime));
    }

  } catch(err){
    console.log(err)
  }
}

module.exports = {
  appInsightTrack,
}

