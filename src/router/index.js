const Router = require('koa-router')
const common = require('../api/common')
const stream = require('../api/stream')

const apiRouter = new Router()

apiRouter.use('/data', common.routes())
apiRouter.use('/stream', stream.routes())

module.exports = apiRouter
