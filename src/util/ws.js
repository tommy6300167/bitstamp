"use strict"
const {ohlcHandler} = require('./calculate')
const WebSocket = require('ws')

const currencyPairList = ['btcusd', 'ethusd', 'ltcusd']
const tickers = {}
const ohlcs = {}
const subscribers = {}

// =============connect with bitstamp socket get data===============
const connectBitstamp = () =>{
  const socket = new WebSocket('wss://ws.bitstamp.net')

  socket.on('open', () => {
    console.log('Connected to Bitstamp WebSocket API')
  
    currencyPairList.forEach(pair =>{
      const subscribeChannel = JSON.stringify({
        event: 'bts:subscribe',
        data: {
          channel: `live_trades_${pair}`
        }
      })
      socket.send(subscribeChannel)
    })
  })
  
  socket.on('message', (data)=>{
    let data2String = data.toString()
    if(typeof data2String === 'string'){
      data2String = JSON.parse(data.toString())
    }
  
    const { channel, data:tickerData } = data2String
  
    if(channel && Object.keys(tickerData).length !== 0){
      const currencyPair = channel.replace('live_trades_', '')
      const price = parseFloat(tickerData.price)
      tickers[currencyPair] = price
      if(!ohlcs[currencyPair]){
        ohlcs[currencyPair] = []
      }
      ohlcs[currencyPair] = ohlcHandler(ohlcs, currencyPair, price)
    }
  })
  
  socket.on('close', () => {
    console.log('Connection closed')
  })
}

//============== Generate socket connection ==============
const init = () => {
  const wss = new WebSocket.Server({ port: 3000 })

  wss.on('connection', (ws) => {
    ws.on('message', (message) =>{
      const data = JSON.parse(message)
      console.log('User connect to ws:', data)
      if (data.type === 'subscribe') {
        const currencyPair = data.data.currencyPairs
        tickers[currencyPair] = 0
        ohlcs[currencyPair] = []
  
        if (!subscribers[ws]) {
          subscribers[ws] = []
        }
        subscribers[ws].push(...currencyPair)
  
      } else if (data.method === 'unsubscribe') {
        delete tickers[data.currencyPair]
        delete ohlcs[data.currencyPair]
      }
    })
  
    // send the new data every ten second
    const intervalId = setInterval(() => {
      if (subscribers[ws] && subscribers[ws].length > 0) {
        const data = {}
        subscribers[ws].forEach((currencyPair) => {
          data[currencyPair] = {
            ticker: tickers[currencyPair],
            ohlc: ohlcs[currencyPair],
          }
        })
        ws.send(JSON.stringify({ type: 'update', data }))
      }
    }, 10000)
  
    // When client disconnected, unsubscribe channle
    ws.on('close', () => {
      if (subscribers[ws]) {
        subscribers[ws].forEach((currencyPair) => {
          delete tickers[currencyPair]
          delete ohlcs[currencyPair]
        })
        delete subscribers[ws]
      }
      clearInterval(intervalId)
    })
  })
}

connectBitstamp()
init()
