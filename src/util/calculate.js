module.exports.ohlcHandler = (ohlcs, currencyPair, price) => {
    const ohlc = ohlcs[currencyPair]
    const isNewMinute = ohlc.length === 0 || Math.floor(Date.now() / 60000) > Math.floor(ohlc[ohlc.length - 1][0] / 60000)
  
    if (isNewMinute) {
      const open = price
      const high = price
      const low = price
      const close = price
      const timestamp = Date.now()
      ohlc.push([timestamp, open, high, low, close])
    } else {
      const [timestamp, open, high, low, close] = ohlc.pop()
      const newHigh = Math.max(high, price)
      const newLow = Math.min(low, price)
      ohlc.push([timestamp, open, newHigh, newLow, price])
    }
    console.log(ohlc)
    return ohlc
  }