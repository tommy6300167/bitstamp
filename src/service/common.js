const axios = require('axios');
const configs = require('../../config')

class DataService {
  async fetchData(ctx) {
    try{
        console.log('user id', ctx.request.query.user)
        console.log('user ip', ctx.request.ip)
        let res = await axios.get(configs.fetchUrl)
        ctx.body = res.data
        ctx.status = 200
    } catch(err){
        ctx.status = 500
    }
  }
}
module.exports = DataService
