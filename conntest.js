// local connection test
const WebSocket = require('ws');
const ws = new WebSocket('ws://localhost:3000/streaming');

ws.addEventListener('open', () => {
  // user use currency pairs to subscribe channel
  ws.send(JSON.stringify({ type: 'subscribe', data: { currencyPairs: ['btcusd'] } }));
});

ws.addEventListener('message', (event) => {
  const { type, data } = JSON.parse(event.data);
  console.log(event.data)
  if (type === 'tickers') {
    console.log('Got tickers:', data);
  } else if (type === 'update') {
    console.log('Got update:', data);
  }
});

ws.addEventListener('close', () => {
  ws.send(JSON.stringify({ type: 'unsubscribe', data: { currencyPairs: ['btcusd', 'ethusd', 'ltcusd'] } }));
});