


const configs= {
    fetchUrl:'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty',
    rataLimit:{
        MAX_IP_REQUESTS: 10,
        MAX_USER_REQUESTS: 5,
        MINUTE_IN_MILLISECONDS: 60000,
    },
    redisConfig:{
        db: 1,
        host: '127.0.0.1',
        port: 6380,
        password: '',
      },
}

module.exports = configs