const Koa = require('koa');
const bodyParser = require('koa-bodyparser')
const Router = require('koa-router')
const app = new Koa();
const apiRouter = require('./src/router')
const logger = require('koa-logger')
const { appInsightTrack } = require('./src/middlewares/userTrack')
const ws = require('./src/util/ws')

app.proxy = true;

app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
  });

app.use(logger())
app.use(bodyParser())

const appRouter = new Router()
appRouter.get('/', ctx => {ctx.body = 'check server alive'})
appRouter.use(appInsightTrack, apiRouter.routes())

app.use(appRouter.routes())

app.listen(3009);